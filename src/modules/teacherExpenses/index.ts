import { BaseEntity } from "../../base/BaseEntity";
import Schema from "./schema";
import { rules } from "./validator";
import { TeacherExpensesDTO } from "./dto";
import { getTeacherExpenses } from "./controller";

export class TeacherExpenses extends BaseEntity {
  // Initializes the routes.
  constructor() {
    super("/teacherexpenses", Schema, rules, TeacherExpensesDTO);
    this.initializeRoutes();
  }
  // Initialize routes.
  private initializeRoutes() {
    // this.router.get(this.path, this.getAllModel);
    this.router.get(this.path, getTeacherExpenses);

    this.router.post(this.path, this.createModel);
    this.router.put(this.path + "/:id", this.updateModel);
    this.router.delete(this.path + "/:id", this.deleteModel);

    // this.router.post(this.path, this.createModel);
  }
}
