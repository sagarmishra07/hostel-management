import { UserDetails } from "./../userDetails/index";
import express, { request } from "express";
//BASE
import { returnData, returnError } from "../../base/BaseFunctions";
import { TeacherExpensesDTO } from "./dto";

//Schema
import schema from "./schema";

// Fetch data from schema.
export const getTeacherExpenses = async (
  request: express.Request,
  response: express.Response
) => {
  try {
    const data: any = await schema.find().populate("userDetails");

    // const data: any = await schema.find().where("createdAt").equals(val);
    //
    // const data: any = await schema.aggregate([
    //   {
    //     $group: {
    //       _id: { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } },
    //       data: { $push: "$$ROOT" },
    //       total: {
    //         $sum: "$total",
    //       },
    //     },
    //   },
    // ]);
    if (!data) throw data;

    return returnData(response, data, "Data Fetched");
  } catch (error: any) {
    return returnError(response, error, error?.message || "Could not get data");
  }
};

// export const getExpensesFromDate = async (
//   request: express.Request,
//   response: express.Response
// ) => {
//   try {
// const data: any = await schema.find();
// const Date = request.params.Date;
// const data: any = await schema.find();
// const data: any = await schema.aggregate([
//   {
//     $lookup: {
//       from: "studentexpenses",
//       localField: "student",
//       foreignField: "_id",
//       as: "data",
//     },
//   },
// ]);

//     console.log(data);
//     if (!data) throw data;

//     return returnData(response, data, "Data Fetched");
//   } catch (error: any) {
//     return returnError(response, error, error?.message || "Could not get data");
//   }
// };

// Adds a new student.
// export const addStudentExpenses = async (
//   request: express.Request,
//   response: express.Response
// ) => {
//   try {
//     const data: any = StudentExpensesDTO.receiver(request.body);
//     const storeStudentExpenses: any = await schema.create([data]);
//     if (!storeStudentExpenses) throw storeStudentExpenses;

//     return returnData(response, storeStudentExpenses, "Student Expenses Added");
//   } catch (error: any) {
//     return returnError(response, error, error?.message || "Could not add data");
//   }
// };

//   const data = await StudentExpensesDTO.receiver(request.body);
//   const session = await schema.startSession();
//   try {
//     await session.startTransaction();

//     const studentExpenses = await schema.create([data], {
//       session,
//     });

//     if (!studentExpenses) throw studentExpenses;

//     if (!studentExpenses) throw studentExpenses;
//     await session.commitTransaction();
//     return returnData(response, studentExpenses, "Added UserExpenses");
//   } catch (error: any) {
//     await session.abortTransaction();
//     return returnError(
//       response,
//       error,
//       error?.message ?? "Could not add expenses"
//     );
//   } finally {
//     await session.endSession();
//   }
// };

// export const deleteStudentExpenses = async (
//   request: express.Request,
//   response: express.Response
// ) => {
//   try {
//     const expensesID = request.params.expensesID;
//     const data = await schema.findByIdAndDelete(expensesID);
//     if (!data) throw data;

//     return returnData(response, data, "Data Deleted");
//   } catch (error: any) {
//     return returnError(
//       response,
//       error,
//       error?.message || "Could not Delete data"
//     );
//   }
// };
// Update student data.

// export const updateStudentExpenses = async (
//   request: express.Request,
//   response: express.Response
// ) => {
//   try {
//     const studentExpenseId: string = request.params.id;
//     const MODIFIED_JSON: any = StudentExpensesDTO.receiver(request.body);
//     await schema.findOneAndUpdate(
//       {
//         _id: studentExpenseId,
//       },
//       MODIFIED_JSON
//     );

//     if (!MODIFIED_JSON) throw MODIFIED_JSON;

//     return returnData(response, MODIFIED_JSON, "Data Updated");
//   } catch (error: any) {
//     return returnError(
//       response,
//       error,
//       error?.message || "Could not update data"
//     );
//   }
// };

export const getStudentByDates = async (
  request: express.Request,
  response: express.Response
) => {
  const paramsVal = request.params.id;
  try {
    const data = await schema.find().populate("userDetails");
    // const value = data.filter((val, index) => {
    //   if (val.userDetails.id === paramsVal) {
    //     return val.userDetails;
    //   }
    // });
    const val = data.filter((value, index) => {
      if (value.userDetails.name.toLowerCase() === paramsVal.toLowerCase()) {
        return value.userDetails;
      }
    });
    return returnData(response, val, "Data Fetched");
  } catch (error: any) {
    return returnError(response, error, error?.message || "Could not get data");
  }
};
