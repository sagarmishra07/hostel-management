import { IUserDetails } from "./../userDetails/dto";
import { IBaseInterface } from "../../base/BaseInterface";
import moment from "moment";
export interface ITeacherExpenses extends IBaseInterface {
  foodCharge: number;
  utilityCharges: number;
  credit: number;
  total: number;
  balance: number;
  due: number;
  expenseDate: Date;
  userDetails: IUserDetails;
}

export const TeacherExpensesDTO = {
  receiver: (data: ITeacherExpenses) => {
    const total = data.foodCharge + data.utilityCharges;

    return {
      foodCharge: data.foodCharge,
      utilityCharges: data.utilityCharges,
      credit: data.credit,
      total: total,

      balance: data.balance,
      userDetails: data.userDetails,
      expenseDate: moment(data.expenseDate).format("YYYY-MM-DD"),
      due: data.balance - total + data.credit,
    };
  },
  sender: (data: ITeacherExpenses) => {
    const total = data.foodCharge + data.utilityCharges + data.credit;

    return {
      id: data._id,
      foodCharge: data.foodCharge,
      utilityCharges: data.utilityCharges,
      credit: data.credit,
      total: total,
      balance: data.balance,
      userDetails: data.userDetails,
      due: data.balance - total,
      expenseDate: moment(data.expenseDate).format("YYYY-MM-DD"),
      createdAt: data.createdAt,
    };
  },
};
