import * as mongoose from "mongoose";

const teacherExpenses = new mongoose.Schema(
  {
    foodCharge: {
      type: Number,
      default: 0,
    },
    utilityCharges: {
      type: Number,
      default: 0,
    },
    credit: {
      type: Number,
      default: 0,
    },
    total: {
      type: Number,
      default: 0,
    },
    balance: {
      type: Number,
      default: 0,
    },
    userDetails: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "UserDetails",
      required: true,
    },
    due: {
      type: Number,
      default: 0,
    },
    expenseDate: {
      type: Date,
      default: Date.now(),
      required: true,
    },
  },
  { timestamps: true }
);

export default mongoose.model<any>("TeacherExpenses", teacherExpenses);
