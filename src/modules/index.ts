// //dont no info

import { UserDetails } from "./userDetails";
import { StudentExpenses } from "./studentExpenses";
import { TeacherExpenses } from "./teacherExpenses";

export const openRoutes = {
  userdetails: new UserDetails().router,
  studentexpenses: new StudentExpenses().router,
  teacherexpenses: new TeacherExpenses().router,
};
