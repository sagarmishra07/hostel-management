import * as mongoose from "mongoose";
import moment from "moment";
// Creates a mongoose schema with a list of users.

const userDetails = new mongoose.Schema(
  {
    firstname: {
      type: String,
      required: true,
    },

    lastname: {
      type: String,
      required: true,
    },
    phoneNumber: {
      type: String,
      required: [true, "Phone Number is required"],
      max: [10, "Phone number is not valid "],
      min: [8, "Phone number is not valid"],
      unique: [true, "Phone number Should be unique"],
    },

    address: {
      type: String,
      required: [true, "Address is required"],
    },

    type: {
      required: true,
      enum: ["Student", "Teacher"],
      default: "Student",
      type: String,
    },
    joinedDate: {
      required: true,
      default: Date.now(),
      type: Date,
    },
  },
  { timestamps: true }
);

export default mongoose.model<any>("UserDetails", userDetails);
