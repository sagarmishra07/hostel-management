import { IBaseInterface } from "../../base/BaseInterface";
import moment from "moment";
// Export interface Iuser to IBaseInterface
export interface IUserDetails extends IBaseInterface {
  firstname: string;
  lastname: string;
  phoneNumber: string;
  address: string;
  joinedDate: Date;
  type: string;
}

export const userDetailsDTO = {
  // Returns a dict with name phone number and address.
  receiver: (data: IUserDetails) => {
    return {
      firstname: data.firstname,
      lastname: data.lastname,

      phoneNumber: data.phoneNumber,
      address: data.address,
      joinedDate: moment(data.joinedDate).format("YYYY-MM-DD"),
      type: data.type,
    };
  },
  // Returns the id name phoneNumber and createdAt values.
  sender: (data: IUserDetails) => {
    return {
      id: data._id,
      firstname: data.firstname,
      lastname: data.lastname,
      phoneNumber: data.phoneNumber,
      address: data.address,
      joinedDate: moment(data.joinedDate).format("YYYY-MM-DD"),
      type: data.type,
      createdAt: data.createdAt,
    };
  },
};
