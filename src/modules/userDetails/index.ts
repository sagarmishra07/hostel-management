import { BaseEntity } from "../../base/BaseEntity";
import { rules } from "./validator";
import { userDetailsDTO } from "./dto";
import { getUserByQuery } from "./controller";
import Schema from "./schema";
// import { addUsers } from "./controller";
// import { updateUser } from "./controller";
// import { deleteUser } from "./controller";
export class UserDetails extends BaseEntity {
  // Initializes the routes.
  constructor() {
    super("/userdetails", Schema, rules, userDetailsDTO);
    this.initializeRoutes();
  }
  // Initialize routes.
  private initializeRoutes() {
    this.router.get(this.path, this.getAllModel);

    this.router.get(this.path + "/search", getUserByQuery);
    this.router.post(this.path, this.createModel);
    this.router.put(this.path + "/:id", this.updateModel);
    this.router.delete(this.path + "/:id", this.deleteModel);
  }
}
