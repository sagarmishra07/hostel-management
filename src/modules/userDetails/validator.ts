// export const rules for a phone number
export const rules = {
  firstname: { required: true, label: "First Name is required" },

  lastname: { required: true, label: "Last Name is required" },
  phoneNumber: { required: true, label: "Phone number" },
  address: { required: true, label: "address" },
};
