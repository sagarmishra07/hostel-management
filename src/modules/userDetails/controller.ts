import express from "express";
//BASE
import { returnData, returnError } from "../../base/BaseFunctions";
import { userDetailsDTO } from "./dto";
//Schema

import moment from "moment";
import schema from "./schema";

// Fetch data from schema.
// export const getUsers = async (
//   request: express.Request,
//   response: express.Response
// ) => {
//   try {
//     const data: any = await schema.find({});
// .populate("expenses");

// const data: any = await schema.aggregate([
// {
//   $lookup: {
//     from: "studentexpenses",
//     localField: "expenses",
//     foreignField: "_id",
//     as: "expenses",
//   },
// },
// {
//   $group: {
//     _id: { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } },
//     data: { $push: "$$ROOT" },
//   },
// },
// ]);
// for (const val of data) {
//   for (const v of val.expenses) {
//     console.log(v.createdAt);
//   }
// }

//     if (!data) throw data;
//     return returnData(response, data, "Data Fetched");
//   } catch (error: any) {
//     return returnError(response, error, error?.message || "Could not get data");
//   }
// };
export const getUserByQuery = async (
  request: express.Request,
  response: express.Response
) => {
  const paramsVal = request.query;
  console.log(paramsVal ? paramsVal : "not found");
  try {
    const data = await schema.find(paramsVal);
    const val = data.map((v: any) => userDetailsDTO.sender(v));

    // const data = await schema.aggregate([
    //   {
    //     $lookup: {
    //       from: "studentexpenses",
    //       localField: "expenses",
    //       foreignField: "_id",
    //       as: "datas",
    //     },
    //   },
    //   {
    //     $group: {
    //       _id: "$address",
    //       productData: { $push: "$$ROOT" },
    //     },
    //   },
    // ]);
    // const data: any = await schema.aggregate([
    //   {
    //     $group: {
    //       _id: "$name",
    //       datas: { $push: "$$ROOT" },
    //     },
    //   },
    // ]);
    if (!val) throw val;
    return returnData(response, val, "Data Fetched");
  } catch (error: any) {
    return returnError(response, error, error?.message || "Could not get data");
  }
};

// Adds a new user.
// export const addUsers = async (
//   request: express.Request,
//   response: express.Response
// ) => {
//   try {
//     const data: any = userDetailsDTO.receiver(request.body);

//     const storeuser: any = await schema.create([data]);
//     if (!storeuser) throw storeuser;

//     return returnData(response, storeuser, "Data Added");
//   } catch (error: any) {
//     return returnError(response, error, error?.message || "Could not add data");
//   }
// };
// Delete a user
// export const deleteUser = async (
//   request: express.Request,
//   response: express.Response
// ) => {
//   try {
//     const user_id = request.params.id;
//     const data = await schema.findByIdAndDelete(user_id);
//     if (!data) throw data;

//     return returnData(response, data, "Data Deleted");
//   } catch (error: any) {
//     return returnError(
//       response,
//       error,
//       error?.message || "Could not Delete data"
//     );
//   }
// };
// Update user data.

// export const updateUser = async (
//   request: express.Request,
//   response: express.Response
// ) => {
//   try {
//     const user_id: string = request.params.id;
//     const MODIFIED_JSON: any = userDetailsDTO.receiver(request.body);
//     await schema.findOneAndUpdate(
//       {
//         _id: user_id,
//       },
//       MODIFIED_JSON
//     );

//     if (!MODIFIED_JSON) throw MODIFIED_JSON;

//     return returnData(response, MODIFIED_JSON, "Data Updated");
//   } catch (error: any) {
//     return returnError(
//       response,
//       error,
//       error?.message || "Could not update data"
//     );
//   }
// };
