import * as mongoose from "mongoose";

const studentExpenses = new mongoose.Schema(
  {
    foodCharge: {
      type: Number,
      default: 0,
    },

    stationaryCharge: {
      type: Number,
      default: 0,
    },

    bookCharge: {
      type: Number,
      default: 0,
    },
    serviceCharge: {
      type: Number,
      default: 0,
    },
    total: {
      type: Number,
      default: 0,
    },

    balance: {
      type: Number,
      default: 0,
    },
    userDetails: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "UserDetails",
      required: true,
    },
    due: {
      type: Number,
      default: 0,
    },
    expenseDate: {
      type: Date,
      default: Date.now(),
      required: true,
    },
  },
  { timestamps: true }
);
export default mongoose.model<any>("StudentExpenses", studentExpenses);
