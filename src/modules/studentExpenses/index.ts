import { BaseEntity } from "../../base/BaseEntity";
import Schema from "./schema";
import { rules } from "./validator";
import { StudentExpensesDTO } from "./dto";
import { getStudentExpenses } from "./controller";
// import { addStudentExpenses } from "./controller";
// import { updateStudentExpenses } from "./controller";
// import { deleteStudentExpenses } from "./controller";
import { getExpensesByDate } from "./controller";
export class StudentExpenses extends BaseEntity {
  // Initializes the routes.
  constructor() {
    super("/studentexpenses", Schema, rules, StudentExpensesDTO);
    this.initializeRoutes();
  }
  // Initialize routes.
  private initializeRoutes() {
    // this.router.get(this.path, this.getAllModel);
    this.router.get(this.path, getStudentExpenses);
    this.router.get(this.path + "/search/studentexpenses", getExpensesByDate);
    this.router.post(this.path, this.createModel);
    this.router.put(this.path + "/:id", this.updateModel);
    this.router.delete(this.path + "/:id", this.deleteModel);

    // this.router.post(this.path, this.createModel);
  }
}
