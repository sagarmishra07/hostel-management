import express, { request } from "express";
//BASE
import { returnData, returnError } from "../../base/BaseFunctions";
import { StudentExpensesDTO } from "./dto";

//Schema
import schema from "./schema";
import { UserDetails } from "../userDetails";

// Fetch data from schema.
export const getStudentExpenses = async (
  request: express.Request,
  response: express.Response
) => {
  try {
    const data: any = await schema
      .find({ type: "Student" })
      .populate("userDetails");
    const val = data.map((v: any) => StudentExpensesDTO.sender(v));
    const filterdExpenses = val.filter((v, index) => {
      try {
        if (val[index].userDetails.type === "Student") {
          return val;
        }
      } catch (error) {
        console.log(error);
      }
    });
    // const data: any = await schema.find().where("createdAt").equals(val);
    //
    // const data: any = await schema.aggregate([
    //   {
    //     $group: {
    //       _id: { $dateToString: { format: "%Y-%m-%d", date: "$createdAt" } },
    //       data: { $push: "$$ROOT" },
    //       total: {
    //         $sum: "$total",
    //       },
    //     },
    //   },
    // ]);
    if (!filterdExpenses) throw filterdExpenses;

    return returnData(response, filterdExpenses, "Data Fetched");
  } catch (error: any) {
    return returnError(response, error, error?.message || "Could not get data");
  }
};

// export const getExpensesFromDate = async (
//   request: express.Request,
//   response: express.Response
// ) => {
//   try {
// const data: any = await schema.find();
// const Date = request.params.Date;
// const data: any = await schema.find();
// const data: any = await schema.aggregate([
//   {
//     $lookup: {
//       from: "studentexpenses",
//       localField: "student",
//       foreignField: "_id",
//       as: "data",
//     },
//   },
// ]);

//     console.log(data);
//     if (!data) throw data;

//     return returnData(response, data, "Data Fetched");
//   } catch (error: any) {
//     return returnError(response, error, error?.message || "Could not get data");
//   }
// };

// Adds a new student.
export const addStudentExpenses = async (
  request: express.Request,
  response: express.Response
) => {
  const data = await StudentExpensesDTO.receiver(request.body);

  const session = await schema.startSession();
  try {
    await session.startTransaction();

    const studentExpenses = await schema.create([data], {
      session,
    });

    if (!studentExpenses) throw studentExpenses;

    if (!studentExpenses) throw studentExpenses;
    await session.commitTransaction();
    return returnData(response, studentExpenses, "Added UserExpenses");
  } catch (error: any) {
    await session.abortTransaction();
    return returnError(
      response,
      error,
      error?.message ?? "Could not add expenses"
    );
  } finally {
    await session.endSession();
  }
};

// export const deleteStudentExpenses = async (
//   request: express.Request,
//   response: express.Response
// ) => {
//   try {
//     const expensesID = request.params.expensesID;
//     const data = await schema.findByIdAndDelete(expensesID);
//     if (!data) throw data;

//     return returnData(response, data, "Data Deleted");
//   } catch (error: any) {
//     return returnError(
//       response,
//       error,
//       error?.message || "Could not Delete data"
//     );
//   }
// };
// Update student data.

// export const updateStudentExpenses = async (
//   request: express.Request,
//   response: express.Response
// ) => {
//   try {
//     const studentExpenseId: string = request.params.id;
//     const MODIFIED_JSON: any = StudentExpensesDTO.receiver(request.body);
//     await schema.findOneAndUpdate(
//       {
//         _id: studentExpenseId,
//       },
//       MODIFIED_JSON
//     );

//     if (!MODIFIED_JSON) throw MODIFIED_JSON;

//     return returnData(response, MODIFIED_JSON, "Data Updated");
//   } catch (error: any) {
//     return returnError(
//       response,
//       error,
//       error?.message || "Could not update data"
//     );
//   }
// };

// export const getStudentByDates = async (
//   request: express.Request,
//   response: express.Response
// ) => {
//   const paramsVal = request.query;
//   try {
//     const data = await schema.find().populate("userDetails");
//     // const value = data.filter((val, index) => {
//     //   if (val.userDetails.id === paramsVal) {
//     //     return val.userDetails;
//     //   }
//     // });

//     return returnData(response, val, "Data Fetched");
//   } catch (error: any) {
//     return returnError(response, error, error?.message || "Could not get data");
//   }
// };

export const getExpensesByDate = async (
  request: express.Request,
  response: express.Response
) => {
  const paramsVal = request.query;

  try {
    const data = await schema
      .find({
        expenseDate: {
          $gte: paramsVal.from,
          $lte: paramsVal.to,
        },
      })
      .populate("userDetails");

    const val = data.map((v: any) => StudentExpensesDTO.sender(v));

    // const data = await schema.aggregate([
    //   {
    //     $lookup: {
    //       from: "studentexpenses",
    //       localField: "expenses",
    //       foreignField: "_id",
    //       as: "datas",
    //     },
    //   },
    //   {
    //     $group: {
    //       _id: "$address",
    //       productData: { $push: "$$ROOT" },
    //     },
    //   },
    // ]);
    // const data: any = await schema.aggregate([
    //   {
    //     $group: {
    //       _id: "$name",
    //       datas: { $push: "$$ROOT" },
    //     },
    //   },
    // ]);
    if (!val) throw val;
    return returnData(response, val, "Data Fetched");
  } catch (error: any) {
    return returnError(response, error, error?.message || "Could not get data");
  }
};
