import { IBaseInterface } from "../../base/BaseInterface";
import { IUserDetails } from "../userDetails/dto";
// Export interface IStudent to IBaseInterface
import moment from "moment";
export interface IStudentExpenses extends IBaseInterface {
  foodCharge: number;

  stationaryCharge: number;
  bookCharge: number;
  serviceCharge: number;
  total: number;
  balance: number;
  userDetails: IUserDetails;
  due: number;
  expenseDate: Date;
}

export const StudentExpensesDTO = {
  // Returns a dict with name phone number and address.

  receiver: (data: IStudentExpenses) => {
    const total =
      data.foodCharge +
      data.stationaryCharge +
      data.bookCharge +
      data.serviceCharge;
    return {
      foodCharge: data.foodCharge,
      stationaryCharge: data.stationaryCharge,
      bookCharge: data.bookCharge,
      serviceCharge: data.serviceCharge,
      total: total,
      balance: data.balance,
      userDetails: data.userDetails,
      expenseDate: moment(data.expenseDate).format("YYYY-MM-DD"),
      // Computes the due charges.
      due: data.balance - total,
    };
  },
  // Returns the id name phoneNumber and createdAt values.
  sender: (data: IStudentExpenses) => {
    const total =
      data.foodCharge +
      data.stationaryCharge +
      data.bookCharge +
      data.serviceCharge;
    return {
      foodCharge: data.foodCharge,
      stationaryCharge: data.stationaryCharge,
      serviceCharge: data.serviceCharge,
      bookCharge: data.bookCharge,
      total: total,
      balance: data.balance,
      userDetails: data.userDetails,
      due: data.balance - data.total,
      expenseDate: moment(data.expenseDate).format("YYYY-MM-DD"),
      createdAt: data.createdAt,
    };
  },
};
