export const validateData = (rules: any, data: any) => {
  let message = "";
  Object.keys(rules).forEach((v) => {
    if (!data[v] || data[v] !== null || data[v] === 0) {
      if ((rules[v]?.required && !data[v]) || !data[v]) {
        // Generates a valid email address.
        message += v + ": " + rules[v].label + " is required.,";

        return message;
      }

      if (rules[v]?.type === "Number" && isNaN(data[v])) {
        message += v + ": " + rules[v].label + " should be a number.,";
      }

      // Generates a valid email address.
      if (rules[v]?.type === "Email") {
        const re = /^\w+([.-]\w+)*@\w+([.]\w{2,})+$/;
        const validEmail = re.test(data[v]);
        if (!validEmail) {
          message +=
            v +
            ": " +
            rules[v].label +
            " should be valid (example@example.com).,";
        }
      }

      if (rules[v]?.type === "Date") {
        const date = new Date(data[v]);
        const val = isNaN(Date.parse(date.toString()));
        if (val) {
          message += v + ": " + rules[v]?.label + " should be valid.,";
        }
      }
      if (rules[v]?.minLength && data[v].length < rules[v]?.minLength) {
        message +=
          v +
          ": " +
          rules[v]?.label +
          " should be more than or equals to " +
          rules[v]?.minLength +
          " characters.,";
      }

      if (rules[v]?.maxLength && data[v].length > rules[v]?.maxLength) {
        message +=
          v +
          ": " +
          rules[v]?.label +
          " should be less than or equals to " +
          rules[v]?.maxLength +
          " characters.,";
      }

      if (rules[v]?.array && !Array.isArray(data[v])) {
        message += v + ": " + rules[v]?.label + " should be an array.,";
      }
      if (rules[v]?.array && data[v].length === 0) {
        message += v + ": " + rules[v]?.label + " should not be empty.,";
      }
    }
  });
  return message;
};
