import { Response } from "express";
import { IBaseInterface } from "./BaseInterface";
import _ from "lodash";

export const throwNotFoundError = (message: string = "Data not found.") => {
  throw new Error(message);
};

export const throwGenericError = (
  message: string = "Could not save data. Try again."
) => {
  throw new Error(message);
};

export const returnData = (
  response: Response,
  data: any,
  message: string,
  status: number = 200
) => {
  response.status(status).json({
    status,
    data,
    message,
  });
};

export const returnError = (
  response: Response,
  errors: any = {},
  message: string,
  status: number = 400
) => {
  console.error("Error in back", errors);

  response.status(status).json({
    status,
    errors,
    message,
  });
};

export const dateAndId = (DTO: IBaseInterface) => {
  return {
    _id: DTO._id,
    createdAt: DTO.createdAt,
    updatedAt: DTO.updatedAt,
  };
};

export const formatErrors = (error: any) => {
  let errors: any = {};
  let message = error;
  const errorFromMongoose = new RegExp("Schema validation failed").exec(error);
  if (error.includes(":")) {
    if (errorFromMongoose) {
      error = error.substring(error.indexOf(":") + 1).trim();
    }
    error = error.split(",");
    error = _.pull(error, "").map((error: string) => error.trim());
    error.forEach((er: any) => {
      const [key, value] = er.split(":").map((error: any) => error.trim());
      errors[key] = value;
    });
    message = "";
  }
  return { errors, message };
};

export const checkModelIfNull = async (
  model: any,
  query: any,
  message: string = ""
) => {
  model = await model.findOne(query);
  if (!model) {
    throw new Error(message);
  }
  return model;
};
